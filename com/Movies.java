package com;

public class Movies {
    private double point;
    private String name,revenue;    

    public double getPoint() {
        return this.point;
    }

    public void setPoint(double point) {
        this.point = point;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }    

    public String getRevenue() {
        return this.revenue;
    }

    public void setRevenue(String revenue) {
        this.revenue = revenue;
    }  

    @Override
    public String toString() {
        return 
        "Name: "+getName()+
        "\nPoint: " + getPoint();        
    }    
}