package com;

import java.util.*;

import com.Action.FastAndFurious;
import com.Action.JohnWick;
import com.Action.Logan;
import com.Comedy.Hangover;
import com.Comedy.ShaunOfDead;
import com.Comedy.YahsiBati;
import com.Horror.It;
import com.Horror.TheConjuring;
import com.SciFi.Endgame;
import com.SciFi.Hobbit;
import com.SciFi.StarWarsIII;

public class Main {

    public static void main(String[] args) {

        ArrayList<Movies> movie = new ArrayList<Movies>();

        // Action
        FastAndFurious fastAndFurious = new FastAndFurious("Rob Cohen", "2001-ABD", false, 106);
        fastAndFurious.setDate("3 April 2001");

        // Wrapper
        String view = "12001356";
        int sayı;
        sayı = Integer.parseInt(view);

        fastAndFurious.setViewer(sayı);
        movie.add(fastAndFurious);
        movie.get(0).setName("Fast & Furious");
        movie.get(0).setPoint(6.6);
        movie.get(0).setRevenue("144.533.925");

        JohnWick johnWick = new JohnWick("David Leitch", false, 96);
        johnWick.setProduction("2014-ABD");
        johnWick.setDate("28 November 2014");
        johnWick.setViewer(14256380);
        movie.add(johnWick);
        movie.get(1).setName("John Wick");
        movie.get(1).setPoint(7.4);
        movie.get(1).setRevenue("86.000.000");

        Logan logan = new Logan("2017-ABD", 137);
        logan.setDirector("James Mangold");
        logan.setNetflix(false);
        logan.setDate("3 March 2017");
        logan.setViewer(8089585);
        movie.add(logan);
        movie.get(2).setName("Logan");
        movie.get(2).setPoint(8.1);
        movie.get(2).setRevenue("226.277.068");

        // Comedy
        Hangover hangover = new Hangover("Todd Phillips", "2009-Germany", true, 100);
        hangover.setDate("10 July 2009");
        hangover.setViewer(1000970);
        movie.add(hangover);
        movie.get(3).setName("Hangover");
        movie.get(3).setPoint(7.7);
        movie.get(3).setRevenue("277.322.503");

        ShaunOfDead shaunOfDead = new ShaunOfDead("2004-UK", false);
        shaunOfDead.setDirector("Edgar Wright");
        shaunOfDead.setMinute(99);
        shaunOfDead.setDate("9 April 2004");
        shaunOfDead.setViewer(1400658);
        movie.add(shaunOfDead);
        movie.get(4).setName("Shaun of the Dead");
        movie.get(4).setPoint(7.9);
        movie.get(4).setRevenue("13.542.874");

        YahsiBati yahsiBati = new YahsiBati("Ömer Faruk Sorak", "1 January 2010", true, 112);
        yahsiBati.setDate("2009-Turkey");
        yahsiBati.setViewer(906663);
        movie.add(yahsiBati);
        movie.get(5).setName("Yahşi Batı");
        movie.get(5).setPoint(7.4);
        movie.get(5).setRevenue("8.431.853");

        // Horor
        It it = new It("Andy Muschietti", "15 September 2017", false, 135);
        it.setDate("2017-ABD");
        it.setViewer(50756249);
        movie.add(it);
        movie.get(6).setName("It");
        movie.get(6).setPoint(7.3);
        movie.get(6).setRevenue("701.796.444");

        TheConjuring theConjuring = new TheConjuring("James Wan", "30 Agust 2013", true, 112);
        theConjuring.setDate("2013-USA");
        theConjuring.setViewer(15458963);
        movie.add(theConjuring);
        movie.get(7).setName("The Conjuring");
        movie.get(7).setPoint(7.5);
        movie.get(7).setRevenue("319.494.638");

        // Sci-Fi
        Endgame endgame;
        endgame = new Endgame("Anthony Russo-Joe Russo", "25 April 2019", false);
        endgame = new Endgame(181);

        endgame.setDate("2019-USA");
        endgame.setViewer(200669745);
        movie.add(endgame);
        movie.get(8).setName("Avengers Endgame");
        movie.get(8).setPoint(8.4);
        movie.get(8).setRevenue("2.797.800.564");

        Hobbit hobbit;
        hobbit = new Hobbit("Peter Jackson", "14 December 2012");
        hobbit = new Hobbit(false, 169);

        hobbit.setDate("2012-USA");
        hobbit.setViewer(100589632);
        movie.add(hobbit);
        movie.get(9).setName("Hobbit");
        movie.get(9).setPoint(7.8);
        movie.get(9).setRevenue("1.017.003.568");

        StarWarsIII starWars;
        starWars = new StarWarsIII("George Lucas", "19 May 2005");
        starWars = new StarWarsIII(false, 140);

        starWars.setDate("2005-USA");
        starWars.setViewer(76895347);
        movie.add(starWars);
        movie.get(10).setName("Star Wars III");
        movie.get(10).setPoint(7.5);
        movie.get(10).setRevenue("868.390.560");
        System.out.println("Welcome to Bartu Kaynar's Project1 \n\t'The Best Movies' ");

        Scanner sc = new Scanner(System.in);
        System.out.println(
                "Please choose a category: \n1.Action\n2.Comedy\n3.Horror\n4.Sci-Fi\n5.All Categories\nPlease enter a number: ");
        boolean chek = true;

        while (chek) {            
            String ssc = sc.nextLine();
            if (ssc.equals("1") || ssc.equals("2") || ssc.equals("3") || ssc.equals("4") || ssc.equals("5")) {
                if (ssc.equals("1")) {
                    for(int i=0; i<=2; i++){
                        System.out.println(movie.get(i));
                    }
                    System.out.println("Press \'0\' for exit");      
                    boolean b = true;  
                    while(b){
                        String a = sc.nextLine(); 
                        if(a.equals("0")){
                            System.exit(0);
                            chek = false;
                            b = false;
                        }else{
                            System.out.println("Please enter a valid value!");
                            b = true;
                        }
                    }            
                } else if (ssc.equals("2")) {
                    for(int i=3; i<=5; i++){
                        System.out.println(movie.get(i));
                    }
                    System.out.println("Press \'0\' for exit");      
                    boolean b = true;  
                    while(b){
                        String a = sc.nextLine(); 
                        if(a.equals("0")){
                            System.exit(0);
                            chek = false;
                            b = false;
                        }else{
                            System.out.println("Please enter a valid value!");
                            b = true;
                        }
                    } 
                } else if (ssc.equals("3")) {
                    for(int i=6; i<=7; i++){
                        System.out.println(movie.get(i));
                    }
                    System.out.println("Press \'0\' for exit");      
                    boolean b = true;  
                    while(b){
                        String a = sc.nextLine(); 
                        if(a.equals("0")){
                            System.exit(0);
                            chek = false;
                            b = false;
                        }else{
                            System.out.println("Please enter a valid value!");
                            b = true;
                        }
                    }
                } else if (ssc.equals("4")) {
                    for(int i=8; i<=10; i++){
                        System.out.println(movie.get(i));
                    }
                    System.out.println("Press \'0\' for exit");      
                    boolean b = true;  
                    while(b){
                        String a = sc.nextLine(); 
                        if(a.equals("0")){
                            System.exit(0);
                            chek = false;
                            b = false;
                        }else{
                            System.out.println("Please enter a valid value!");
                            b = true;
                        }
                    }
                } else if (ssc.equals("5")) {
                    for (Movies mov : movie){ System.out.println(mov); }
                    System.out.println("Press \'0\' for exit");                    
                    boolean b = true;  
                    while(b){
                        String a = sc.nextLine(); 
                        if(a.equals("0")){
                            System.exit(0);
                            chek = false;
                            b = false;
                        }else{
                            System.out.println("Please enter a valid value!");
                            b = true;
                        }
                    }
                }
            } else {
                System.out.println("Please enter number between 1-5!");
                chek = true;
            }
      }  }
}