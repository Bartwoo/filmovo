package com.Horror;

import com.Movies;

public class Horror extends Movies {
    private int viewer;
    private String date;

    public int getViewer() {
        return this.viewer;
    }

    public void setViewer(int viewer) {
        this.viewer = viewer;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return
        "Viewer: " + getViewer()+
        "\nDate: " + getDate();
    }
}