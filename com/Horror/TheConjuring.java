package com.Horror;

public class TheConjuring extends Horror{
    private String director,production;
    private boolean netflix;
    private int minute; 

    public TheConjuring(String director, String production, boolean netflix, int minute) {
        this.director = director;
        this.production = production;
        this.netflix = netflix;
        this.minute = minute;
    }

    public String getDirector() {
        return this.director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getProduction() {
        return this.production;
    }

    public void setProduction(String production) {
        this.production = production;
    }

    public boolean isNetflix() {
        return this.netflix;
    }

    public boolean getNetflix() {
        return this.netflix;
    }

    public void setNetflix(boolean netflix) {
        this.netflix = netflix;
    }

    public int getMinute() {
        return this.minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    @Override
    public String toString() {
        return 
        "\nName: " + super.getName() +
        "\nPoint: " + super.getPoint() +
        "\nVision Date" + super.getDate() +
        "\nMinute: " + getMinute() + "m" +
        "\nDirector: " + getDirector() +
        "\nProduction: " + getProduction() +
        "\nNetflix: " + isNetflix()+
        "\nRevenue: " + super.getRevenue() + " $" +
        "\nViews: " + super.getViewer() + "\n";
    }
}